# bitrading-module-manager

`version` : `3.1.3`

## Methods

### basic getters
- `version`
- `assertEnv`
- `ResolveError`
- `resolveValidator`
- `askingPrice`

### data getters
- `validatorTemplate`
- `errorTemplate`

### applied getters
- `validator`
- `error`

### api getters
api 는 보안상의 이유로 지원하지 않습니다.
@bitrading-api/module 으로 직접 추가하여 사용해야 합니다.

## Versions

### v3.1.2
- `bitrading-validator-template` 버전 업데이트 -> 0.3.0

### v3.1.2
- `askingPrice` 패키지 누락 추가

### v3.1.1
- bitrading-validator-template 버전 변경 0.1 -> 0.2

### v3.1
- `askingPrice` 모듈 추가

### v3.0.0
- 보안상 이유로 api 지원 종료

### v2.0.1
- 버그 수정

### v2.0.0
- API 내용 변경 : 생성자가 있는 class일 경우 CamelCase, 그 외일 경우 camelCase 로 지정한다.
- v1.1 에서의 이름은 `resolveError`->`ResolveError, `upbitApi`->`UpbitApi`, `bitradingApi`->`BitradingApi` 으로 변경되었다.

### v1.1.1
- 필수 모듈 내장
- 패키지 이름 변경. @bitrading/bitrading-module-manager -> bitrading-module-manager

### v1.1.0
- local_modules & local_data 경로 지원 안함(submodule이 아닌 package.json를 이용한 형상관리)
- Package.json name with @bitrading group

### v1.0.0
- local_modules & local_data 경로 지원
