/**
 * @fileOverview 비트레이딩 여러 모듈을 한번에 관리하는 모듈
 * @name bitrading-module-manager
 * @author https://bitrading.kr
 * @date 2018-10-30
 * @version 3.1.3
 */

const VERSION = '3.1.3';

class ModuleManager {
  get version () {
    return VERSION;
  }
  // basic
  get assertEnv () {
    if (this._assertEnv == null) {
      this._assertEnv = require('@bitrading/assert-env');
    }
    return this._assertEnv;
  }
  get ResolveError () {
    if (this._ResolveError == null) {
      this._ResolveError = require('@bitrading/resolve-error');
    }
    return this._ResolveError;
  }
  get resolveValidator () {
    if (this._resolveValidator == null) {
      this._resolveValidator = require('@bitrading/resolve-validator');
    }
    return this._resolveValidator;
  }
  get askingPrice () {
    if (this._askingPrice == null) {
      this._askingPrice = require('@bitrading/asking-price');
    }
    return this._askingPrice;
  }

  // data
  get validatorTemplate () {
    if (this._validatorTemplate == null) {
      this._validatorTemplate = require('@bitrading-data/bitrading-validator-template');
    }
    return this._validatorTemplate;
  }
  get errorTemplate () {
    if (this._errorTemplate == null) {
      this._errorTemplate = require('@bitrading-data/bitrading-error-template');
    }
    return this._errorTemplate;
  }

  // applied
  get validator () {
    if (this._validator == null) {
      this._validator = this.resolveValidator(this.validatorTemplate);
    }
    return this._validator;
  }
  get error () {
    if (this._error == null) {
      this._error = new this.ResolveError(this.errorTemplate.ErrorList, this.errorTemplate.CommonStatus, this.errorTemplate.CommonReason);
    }
    return this._error;
  }
}

module.exports = new ModuleManager();
